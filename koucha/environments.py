# -*- coding: utf-8 -*-
import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

WEBDRIVER_PATH = os.environ.get('WEBDRIVER_PATH')
GITHUB_USERNAME = os.environ.get('GITHUB_USERNAME')
GITHUB_PASSWORD = os.environ.get('GITHUB_PASSWORD')
WAIT_TIMEOUT = os.environ.get('WAIT_TIMEOUT', 10)
