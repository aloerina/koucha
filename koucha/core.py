# -*- coding: utf-8 -*-
import logging
from pathlib import Path
from dotenv import load_dotenv, find_dotenv
from logging.handlers import RotatingFileHandler
from hackmd import HackMD
from environments import (
    WEBDRIVER_PATH,
    GITHUB_USERNAME,
    GITHUB_PASSWORD,
    WAIT_TIMEOUT
)

def set_root_logger(filename, is_debug=False):
    """rootロガーを設定する

    Args:
        filename(str): ログファイルのファイル名
        is_debug(bool): デバッグモードならTrue
    """
    # rootロガー取得
    logger = logging.getLogger()
    # ハンドラ作成：10MBのサイズローテートで5世代管理
    handler = RotatingFileHandler(
        filename=filename,
        maxBytes=10000000,
        backupCount=5,
        encoding='utf-8'
    )
    formatter = logging.Formatter('%(asctime)s %(name)s [%(levelname)s] %(message)s')
    handler.setFormatter(formatter)
    # rootロガー設定
    if is_debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logger.addHandler(handler)

def main(is_debug):
    load_dotenv(find_dotenv())
    logdir = Path('log')
    logdir.mkdir(exist_ok=True)
    logfile = logdir / 'koucha.log'
    set_root_logger(logfile, is_debug)
    logger = logging.getLogger(__name__)
    logger.debug('Koucha debug logging.')
    print('Koucha Start')
    try:
        hackmd = HackMD(WEBDRIVER_PATH, WAIT_TIMEOUT)
        hackmd.sign_in_by_github_oauth(GITHUB_USERNAME, GITHUB_PASSWORD)
        notes = hackmd.get_notes()
        print(notes)
    finally:
        hackmd.quit()
