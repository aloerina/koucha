# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

class HackMD:
    def __init__(self, webdriver_path, timeout):
        options = webdriver.ChromeOptions()
        # options.add_argument('--headless')
        # options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(webdriver_path, chrome_options=options)
        self.timeout = timeout
        self.base_url = 'https://hackmd.io'

    def _connect(self):
        self.driver.get(self.base_url)

    def _wait(self, timeout=None):
        if not timeout:
            timeout = self.timeout
        return WebDriverWait(self.driver, self.timeout)

    def sign_in_by_github_oauth(self, username, password):
        self._connect()

        self.driver.get(self.base_url + '/auth/github')
        self._wait().until(EC.presence_of_all_elements_located)

        # Github oauth process
        username_fieled = self.driver.find_element_by_id('login_field')
        username_fieled.clear()
        username_fieled.send_keys(username)
        password_field = self.driver.find_element_by_id('password')
        password_field.clear()
        password_field.send_keys(password)
        self.driver.find_element_by_name('commit').click()
        self._wait().until(EC.presence_of_all_elements_located)

        return self._is_sign_in_hackmd()

    def _is_sign_in_hackmd(self):
        # Aleady Signed in if <div class="ui-signout"> is exist.
        return True if self.driver.find_elements_by_class_name('ui-signout') else False

    def quit(self):
        self.driver.quit()

    def new_note(self, markdown):
        self.driver.get(self.base_url + '/new')
        self._wait().until(EC.presence_of_all_elements_located)
        codemirror_textarea = self.driver.find_element_by_css_selector('.CodeMirror textarea')
        self._wait().until(EC.presence_of_element_located((By.CLASS_NAME, 'CodeMirror-focused')))
        codemirror_textarea.send_keys(markdown)

    def get_notes(self, include_md=False):
        self.driver.get(self.base_url)
        self._wait().until(EC.presence_of_all_elements_located)
        categories_toggle = self.driver.find_element_by_css_selector('.categories-toggle .switch input[type=checkbox]')
        if categories_toggle.is_selected():
            categories_slider = self.driver.find_element_by_css_selector('.slider.round')
            categories_slider.click()
            self._wait().until(EC.presence_of_element_located((By.CLASS_NAME, 'list-section')))

        note_elems = self.driver.find_elements_by_css_selector('.list-section .list li')
        return [self._parse_note(note_elem, include_md) for note_elem in note_elems]

    def _parse_note(self, element, include_me=False):
        # You can't  `.text` access because target tag has `display:none` css property.
        # In case you must use `get_attribute('textContent')` method.
        note_id = element.find_element_by_class_name('id').get_attribute('textContent')
        title = element.find_element_by_css_selector('.content .text').text
        link = element.find_element_by_css_selector('a').get_attribute('href')
        tag_elems = element.find_elements_by_css_selector('.tags .label')
        tags = []
        for tag_elem in tag_elems:
            tag_title = tag_elem.get_attribute('title')
            # hidden tags
            if not tag_title:
                ActionChains(self.driver).move_to_element(tag_elem).perform()
                self._wait().until(EC.presence_of_element_located((By.CLASS_NAME, 'popover-content')))
                hidden_tags = tag_elem.find_elements_by_css_selector('.popover-content .label')
                tags.extend([tag.get_attribute('title') for tag in hidden_tags])
            else:
                tags.append(tag_title)

        meta = HackMDNoteMeta(note_id, title, link, tags)
        if not include_me:
            return meta
        else:
            markdown = self._get_markdown(note_id)
            return HackMDNote(meta, markdown)

    def _get_markdown(self, note_id):
        self.driver.get(self.base_url + '/{}'.format(note_id))
        self._wait().until(EC.presence_of_all_elements_located)
        markdown = ""
        return markdown


class HackMDNoteMeta:
    def __init__(self, note_id, title, link, tags):
        self.id = note_id
        self.title = title
        self.link = link
        self.tags = tags

    def __repr__(self):
        return "<HackMDNoteMeta id:{}, title:{}, link:{}, tags:{}>".format(
            self.id, self.title, self.link, self.tags
        )

class HackMDNote(HackMDNoteMeta):
    def __init__(self, meta, markdown):
        super().__init__(meta.note_id, meta.title, meta.link, meta.tags)
        self.markdown = markdown

    def __repr__(self):
        return "<HackMDNote id:{}, title:{}, link:{}, tags:{}, markdown:{}>".format(
            self.id, self.title, self.link, self.tags, self.markdown
        )
