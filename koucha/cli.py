# -*- coding: utf-8 -*-
import click
from core import main as _main

@click.command()
@click.option('--debug',
              'is_debug',
              is_flag=True,
              default=False,
              help='debug mode (default: False)')
def main(is_debug):
    _main(is_debug)
